from collections import deque

class ReSyntaxError(Exception):
    def __init__(self, position, text):
        self.position = position
        self.text = text

    def __str__(self):
        return '\n  ' + self.text + \
               '\n  ' + ' ' * self.position + '^' + '\n'
          
def parse_re(re):
    def fail(p):
        raise ReSyntaxError(p, re)

    def parse_atomic(p):
        if re[p] == '$':
            return p + 1, ['$']
        if re[p] in '()+*':
            fail(p)
        return p + 1, ['@', re[p]]

    def parse_parenthesized(p):
        assert re[p] == '('
        p += 1

        p, x = parse_alternatives(p)

        if p < len(re) and re[p] == ')':
            p += 1
        else:
            fail(p)

        return p, x

    def parse_primary(p):
        if re[p] == '(':
            return parse_parenthesized(p)
        return parse_atomic(p)

    def parse_repetition(p):
        p, x = parse_primary(p)

        while p < len(re) and re[p] == '*':
            p += 1
            x = ['*', x]

        return p, x

    def parse_sequence(p):
        p, x = parse_repetition(p)

        while p < len(re) and re[p] not in '+)':
            p, y = parse_repetition(p)
            x = ['~', x, y]

        return p, x

    def parse_alternatives(p):
        if p == len(re):
            fail(p)

        p, x = parse_sequence(p)

        while p < len(re) and re[p] == '+':
            p += 1
            p, y = parse_sequence(p)
            x = ['|', x, y]

        return p, x

    p, x = parse_alternatives(0)
    if p < len(re):
        fail(p)
    return x

def parsed_re_to_nfa(re):
    state_count = 0
    transitions = {}

    def make_state():
        nonlocal state_count

        state = state_count
        state_count += 1
        transitions[state] = {}
        return state

    def add_transition(origin, gate, target):
        transitions[origin].setdefault(gate, set()).add(target)

    def convert(re):
        if re[0] == '$':
            start = make_state()
            end = make_state()

            add_transition(start, '', end)

            return start, end

        if re[0] == '@':
            start = make_state()
            end = make_state()

            add_transition(start, re[1], end)

            return start, end

        if re[0] == '*':
            new_start = make_state()
            start, end = convert(re[1])
            new_end = make_state()

            add_transition(new_start, '', start)
            add_transition(new_start, '', new_end)
            add_transition(end, '', new_end)
            add_transition(end, '', start)

            return new_start, new_end

        if re[0] == '~':
            first_start, first_end = convert(re[1])
            second_start, second_end = convert(re[2])

            add_transition(first_end, '', second_start)

            return first_start, second_end

        if re[0] == '|':
            new_start = make_state()
            first_start, first_end = convert(re[1])
            second_start, second_end = convert(re[2])
            new_end = make_state()

            add_transition(new_start, '', first_start)
            add_transition(new_start, '', second_start)
            add_transition(first_end, '', new_end)
            add_transition(second_end, '', new_end)

            return new_start, new_end

    start, end = convert(re)

    return {
        'start': start,
        'transitions': transitions,
        'end': end,
    }

def nfa_to_dfa(nfa):
    alphabet = frozenset(g for s, ts in nfa['transitions'].items()
                           for g in ts
                           if g != '')

    def compute_closure(state):
        states = set()

        queue = deque([state])
        while queue:
            origin = queue.popleft()
            for target in nfa['transitions'][origin].get('', ()):
                if target in states:
                    continue
                queue.append(target)
            states.add(origin)

        return frozenset(states)

    def step(origins, symbol):
        targets = set()

        for origin in origins:
            for target in nfa['transitions'][origin].get(symbol, ()):
                targets |= compute_closure(target)

        return frozenset(targets)

    states = {}
    nfa_state_sets = {}
    transitions = {}
    ends = set()

    queue = deque()

    def get_state(nfa_state_set):
        try:
            return states[nfa_state_set]
        except KeyError:
            pass

        state = states[nfa_state_set] = len(states)
        nfa_state_sets[state] = nfa_state_set
        transitions[state] = {}
        if nfa['end'] in nfa_state_set:
            ends.add(state)

        queue.append(state)

        return state

    start = get_state(compute_closure(nfa['start']))

    while queue:
        state = queue.popleft()
        for symbol in alphabet:
            transitions[state][symbol] = get_state(step(nfa_state_sets[state], symbol))

    return {
        'start': start,
        'transitions': transitions,
        'ends': ends,
    }

def minimize_dfa(dfa):
    start = dfa['start']
    transitions = dfa['transitions']
    ends = frozenset(dfa['ends'])
    non_ends = frozenset(transitions) - ends
    alphabet = frozenset(transitions[0])

    if not ends or not non_ends:
        return dfa

    state_sets = {ends, non_ends}
    queue = {ends}
    while queue:
        targets = queue.pop()
        for symbol in alphabet:
            origins = set();
            for state in transitions:
                if transitions[state][symbol] in targets:
                    origins.add(state)

            next_state_sets = set()
            for state_set in state_sets:
                inners = state_set & origins
                if not inners:
                    next_state_sets.add(state_set)
                    continue

                outers = state_set - origins
                if not outers:
                    next_state_sets.add(state_set)
                    continue

                next_state_sets.add(inners)
                next_state_sets.add(outers)

                if state_set in queue:
                    queue.remove(state_set)
                    queue.add(inners)
                    queue.add(outers)
                else:
                    if len(inners) < len(outers):
                        queue.add(inners)
                    else:
                        queue.add(outers)
            state_sets = next_state_sets

    dfa_start = start
    dfa_transitions = transitions
    dfa_ends = ends

    states = {dfa_state_set: state for state, dfa_state_set in enumerate(state_sets)}
    dfa_state_sets = {state: dfa_state_set for dfa_state_set, state in states.items()}
    start = None
    transitions = {}
    ends = set()
    for state in dfa_state_sets:
        if dfa_start in dfa_state_sets[state]:
            if start is None:
                start = state
            else:
                assert start == state
        for dfa_state in dfa_state_sets[state]:
            if state not in transitions:
                transitions[state] = {}
            for symbol, dfa_target in dfa_transitions[dfa_state].items():
                for target in dfa_state_sets:
                    if dfa_target in dfa_state_sets[target]:
                        if symbol not in transitions[state]:
                            transitions[state][symbol] = target
                        else:
                            assert transitions[state][symbol] == target
        if dfa_ends & dfa_state_sets[state]:
            ends.add(state)

    return {
        'start': start,
        'transitions': transitions,
        'ends': ends,
    }


import sys
import readline

def pprint_titled(title, value):
    from pprint import pformat
    from textwrap import indent
    print(title + ':')
    print(indent(pformat(value), ' ' * 2))

while True:
    re = input('RE: ')
    if not re:
        break

    try:
        parsed_re = parse_re(re)
    except ReSyntaxError as e:
        print('Syntax error:' + str(e), file=sys.stderr)
        continue

    nfa = parsed_re_to_nfa(parsed_re)
    pprint_titled('NFA', nfa)

    dfa = nfa_to_dfa(nfa)
    pprint_titled('DFA', dfa)

    mdfa = minimize_dfa(dfa)
    pprint_titled('Minimized DFA', mdfa)

    start = mdfa['start']
    transitions = mdfa['transitions']
    ends = mdfa['ends']
    alphabet = set(transitions[0])

    print()

    while True:
        try:
            string = input('String to match: ')
        except EOFError:
            break

        bad_symbols = {s for s in string if s not in alphabet}
        if bad_symbols:
            print('Error: characters {} are not in alphabet {}.'.format(repr(''.join(sorted(bad_symbols))),
                                                                        repr(''.join(sorted(alphabet)))), file=sys.stderr)
            continue

        print('Execution:')
        state = start
        for symbol in string:
            origin = state
            target = transitions[origin][symbol]
            print(' {:>3} --{}--> {}'.format(origin, repr(symbol), target))
            state = target
        if state in ends:
            print('Done. String matches.')
        else:
            print('Done. String does not match.')

        print()

    print()
    print()
